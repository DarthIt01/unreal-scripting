// Fill out your copyright notice in the Description page of Project Settings.


#include "MySecondActor.h"

// Sets default values
AMySecondActor::AMySecondActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Creates a StaticMeshComponent on this object and assigns Mesh to it
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>("BaseMeshComponent");

	auto MeshAsset = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("StaticMesh'/Engine/BasicShapes/Sphere.Sphere'"));

	//Check if the MeshAsset is valid before setting it
	if(MeshAsset.Object != nullptr)
	{
		Mesh->SetStaticMesh(MeshAsset.Object);
	}

}

// Called when the game starts or when spawned
void AMySecondActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMySecondActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

