// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Chapter04.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Chapter04, "Chapter04" );
