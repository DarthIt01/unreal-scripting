// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.


#include "Chapter04GameModeBase.h"
#include "InventoryCharacter.h"

AChapter04GameModeBase::AChapter04GameModeBase()
{
	DefaultPawnClass = AInventoryCharacter::StaticClass();
}

void AChapter04GameModeBase::BeginPlay()
{
	Super::BeginPlay();

	//PlayerController = GetWorld()->GetFirstPlayerController();
}