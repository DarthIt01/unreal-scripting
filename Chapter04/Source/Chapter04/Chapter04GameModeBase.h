// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Chapter04GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CHAPTER04_API AChapter04GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	

public:

	AChapter04GameModeBase();

	virtual void BeginPlay() override;

	//class APlayerController* PlayerController;

};
