// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Chapter13GameMode.h"
#include "Chapter13Character.h"
#include "UObject/ConstructorHelpers.h"

AChapter13GameMode::AChapter13GameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
