// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyAIController.h"
#include "EnemyCharacter.h"
#include "BehaviorTree/BehaviorTree.h"

AEnemyAIController::AEnemyAIController()
{
	//Initialize components
	BehaviorComp = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviorComp"));
	BlackboardComp = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackboardComp"));
}

//Called when the controller possesses a Pawn/Character
void AEnemyAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	auto pCharacter = Cast<AEnemyCharacter>(InPawn);

	//Check if pointers are valid
	if (pCharacter && pCharacter->EnemyBehaviorTree)
	{
		BlackboardComp->InitializeBlackboard(*pCharacter->EnemyBehaviorTree->BlackboardAsset);

		TargetKeyID = BlackboardComp->GetKeyID("Target");

		BehaviorComp->StartTree(*pCharacter->EnemyBehaviorTree);
	}
}