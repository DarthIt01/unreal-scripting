// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Chapter13GameMode.generated.h"

UCLASS(minimalapi)
class AChapter13GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AChapter13GameMode();
};



