// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Chapter10GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CHAPTER10_API AChapter10GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
