// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Chapter10_Editor.h"
#include "Framework/Commands/Commands.h"
#include "EditorStyleSet.h"


class FCookbookCommands: public TCommands<FCookbookCommands>
{
public:
	FCookbookCommands() : TCommands<FCookbookCommands>(FName(TEXT("UE4_Cookbook")), FText::FromString("Cookbook Commands"), NAME_None, FEditorStyle::GetStyleSetName()) {};

	virtual void RegisterCommands() override;

	TSharedPtr<FUICommandInfo> MyButton;

	TSharedPtr<FUICommandInfo> MyMenuButton;

	TSharedPtr<FUICommandInfo> MyMenuButton2;

	TSharedPtr<FUICommandInfo> MyButton4;
};