// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Chapter10_Editor.h"
#include "MyCustomAssetActions.h"
#include "PropertyEditorModule.h"
#include "MyCustomAssetDetailsCustomization.h"

IMPLEMENT_GAME_MODULE(FChapter10_EditorModule, Chapter10_Editor);

void FChapter10_EditorModule::MyButton_Clicked()
{
	TSharedRef<SWindow> CookbookWindow = SNew(SWindow)
		.Title(FText::FromString(TEXT("Cookbook Window")))
		.ClientSize(FVector2D(800, 400))
		.SupportsMaximize(false)
		.SupportsMinimize(false);

	IMainFrameModule& MainFrameModule = FModuleManager::LoadModuleChecked<IMainFrameModule>(TEXT("MainFrame"));

	if (MainFrameModule.GetParentWindow().IsValid())
	{
		FSlateApplication::Get().AddWindowAsNativeChild(CookbookWindow, MainFrameModule.GetParentWindow().ToSharedRef());
	}
	else
	{
		FSlateApplication::Get().AddWindow(CookbookWindow);
	}
}

void FChapter10_EditorModule::AddToolbarExtension(FToolBarBuilder& builder)
{
	FSlateIcon IconBrush = FSlateIcon(FEditorStyle::GetStyleSetName(), "LevelEditor.ViewOptions", "LevelEditor.ViewOptions.Small");
	builder.AddToolBarButton(FCookbookCommands::Get().MyButton, NAME_None, FText::FromString("My Button"), FText::FromString("Click me to display a message"), IconBrush, NAME_None);
}

void FChapter10_EditorModule::StartupModule()
{
	FCookbookCommands::Register();
	TSharedPtr<FUICommandList> CommandList = MakeShareable(new FUICommandList());
	CommandList->MapAction(FCookbookCommands::Get().MyButton, FExecuteAction::CreateRaw(this, &FChapter10_EditorModule::MyButton_Clicked), FCanExecuteAction());

	ToolbarExtender = MakeShareable(new FExtender());
	FLevelEditorModule& LevelEditorModule = FModuleManager::LoadModuleChecked<FLevelEditorModule>("LevelEditor");
	Extension = ToolbarExtender->AddToolBarExtension("Compile", EExtensionHook::Before, CommandList, FToolBarExtensionDelegate::CreateRaw(this, &FChapter10_EditorModule::AddToolbarExtension));

	LevelEditorModule.GetToolBarExtensibilityManager()->AddExtender(ToolbarExtender);



	CommandList->MapAction(FCookbookCommands::Get().MyMenuButton, FExecuteAction::CreateRaw(this, &FChapter10_EditorModule::MyMenuButton_Clicked), FCanExecuteAction());

	MenuExtender = MakeShareable(new FExtender());
	Extension2 = MenuExtender->AddMenuExtension("LevelEditor", EExtensionHook::Before, CommandList, FMenuExtensionDelegate::CreateRaw(this, &FChapter10_EditorModule::AddMenuExtension));

	LevelEditorModule.GetMenuExtensibilityManager()->AddExtender(MenuExtender);



	CommandList->MapAction(FCookbookCommands::Get().MyMenuButton2, FExecuteAction::CreateRaw(this, &FChapter10_EditorModule::MyMenuButton2_Clicked), FCanExecuteAction());

	MenuExtender2 = MakeShareable(new FExtender());
	Extension3 = MenuExtender2->AddMenuExtension("General", EExtensionHook::Before, CommandList, FMenuExtensionDelegate::CreateRaw(this, &FChapter10_EditorModule::AddMenuExtension2));

	LevelEditorModule.GetMenuExtensibilityManager()->AddExtender(MenuExtender2);



	CommandList->MapAction(FCookbookCommands::Get().MyButton4, FExecuteAction::CreateRaw(this, &FChapter10_EditorModule::MyButton4_Clicked), FCanExecuteAction());

	ToolbarExtender4 = MakeShareable(new FExtender());
	
	Extension4 = ToolbarExtender4->AddToolBarExtension("Misc", EExtensionHook::Before, CommandList, FToolBarExtensionDelegate::CreateRaw(this, &FChapter10_EditorModule::AddToolbarExtension4));

	LevelEditorModule.GetToolBarExtensibilityManager()->AddExtender(ToolbarExtender4);


	IAssetTools& AssetTools = FModuleManager::LoadModuleChecked<FAssetToolsModule>("AssetTools").Get();
	auto Actions = MakeShareable(new FMyCustomAssetActions);
	AssetTools.RegisterAssetTypeActions(Actions);
	CreatedAssetTypeActions.Add(Actions);



	DisplayTestCommand = IConsoleManager::Get().RegisterConsoleCommand(TEXT("DisplayTestCommand"), TEXT("test"), FConsoleCommandDelegate::CreateRaw(this, &FChapter10_EditorModule::DisplayWindow, FString(TEXT("Test Command Window"))), ECVF_Default);

	DisplayUserSpecifiedWindow = IConsoleManager::Get().RegisterConsoleCommand(TEXT("DisplayWindow"), TEXT("test"), FConsoleCommandWithArgsDelegate::CreateLambda([&](const TArray< FString >& Args) {
		FString WindowTitle;
		for (FString Arg : Args)
		{
			WindowTitle += Arg;
			WindowTitle.AppendChar(' ');
		}
		this->DisplayWindow(WindowTitle);
		}
	), ECVF_Default);
	
	PinFactory = MakeShareable(new FMyCustomAssetPinFactory());
	FEdGraphUtilities::RegisterVisualPinFactory(PinFactory);



	FPropertyEditorModule& PropertyModule = FModuleManager::LoadModuleChecked<FPropertyEditorModule>("PropertyEditor");
	PropertyModule.RegisterCustomClassLayout(UMyCustomAsset::StaticClass()->GetFName(), FOnGetDetailCustomizationInstance::CreateStatic(&FMyCustomAssetDetailsCustomization::MakeInstance));
	PropertyModule.RegisterCustomPropertyTypeLayout(UMyCustomAsset::StaticClass()->GetFName(), FOnGetPropertyTypeCustomizationInstance::CreateStatic(&FMyCustomAssetPropertyDetails::MakeInstance));
}

void FChapter10_EditorModule::ShutdownModule()
{
	ToolbarExtender->RemoveExtension(Extension.ToSharedRef());
	Extension.Reset();
	ToolbarExtender.Reset();

	MenuExtender->RemoveExtension(Extension2.ToSharedRef());
	Extension2.Reset();
	MenuExtender.Reset();

	MenuExtender2->RemoveExtension(Extension3.ToSharedRef());
	Extension3.Reset();
	MenuExtender2.Reset();

	ToolbarExtender4->RemoveExtension(Extension4.ToSharedRef());
	Extension4.Reset();
	ToolbarExtender4.Reset();

	IAssetTools& AssetTools = FModuleManager::LoadModuleChecked<FAssetToolsModule>("Asset Tools").Get();

	for (auto Action : CreatedAssetTypeActions)
	{
		AssetTools.UnregisterAssetTypeActions(Action.ToSharedRef());
	}

	if (DisplayTestCommand)
	{
		IConsoleManager::Get().UnregisterConsoleObject(DisplayTestCommand);
		DisplayTestCommand = nullptr;
	}

	if (DisplayUserSpecifiedWindow)
	{
		IConsoleManager::Get().UnregisterConsoleObject(DisplayUserSpecifiedWindow);
		DisplayUserSpecifiedWindow = nullptr;
	}

	FEdGraphUtilities::UnregisterVisualPinFactory(PinFactory);
	PinFactory.Reset();

	FPropertyEditorModule& PropertyModule = FModuleManager::LoadModuleChecked<FPropertyEditorModule>("PropertyEditor");
	PropertyModule.UnregisterCustomClassLayout(UMyCustomAsset::StaticClass()->GetFName());
		
}

void FChapter10_EditorModule::AddMenuExtension(FMenuBuilder& builder)
{
	FSlateIcon IconBrush = FSlateIcon(FEditorStyle::GetStyleSetName(), "LevelEditor.ViewOptions", "LevelEditor.ViewOptions.Small");
	builder.AddMenuEntry(FCookbookCommands::Get().MyMenuButton);
}

void FChapter10_EditorModule::MyMenuButton_Clicked()
{
	TSharedRef<SWindow> CookbookWindow = SNew(SWindow)
		.Title(FText::FromString(TEXT("Cookbook Window")))
		.ClientSize(FVector2D(800, 400))
		.SupportsMaximize(false)
		.SupportsMinimize(false);

	IMainFrameModule& MainFrameModule = FModuleManager::LoadModuleChecked<IMainFrameModule>(TEXT("MainFrame"));

	if (MainFrameModule.GetParentWindow().IsValid())
	{
		FSlateApplication::Get().AddWindowAsNativeChild(CookbookWindow, MainFrameModule.GetParentWindow().ToSharedRef());
	}
	else
	{
		FSlateApplication::Get().AddWindow(CookbookWindow);
	}
}

void FChapter10_EditorModule::AddMenuExtension2(FMenuBuilder& builder)
{
	FSlateIcon IconBrush = FSlateIcon(FEditorStyle::GetStyleSetName(), "LevelEditor.ViewOptions", "LevelEditor.ViewOptions.Small");
	builder.AddMenuEntry(FCookbookCommands::Get().MyMenuButton2);
}

void FChapter10_EditorModule::MyMenuButton2_Clicked()
{
	TSharedRef<SWindow> CookbookWindow = SNew(SWindow)
		.Title(FText::FromString(TEXT("Cookbook Menu Window2")))
		.ClientSize(FVector2D(600, 600))
		.SupportsMaximize(false)
		.SupportsMinimize(false);

	IMainFrameModule& MainFrameModule = FModuleManager::LoadModuleChecked<IMainFrameModule>(TEXT("MainFrame"));

	if (MainFrameModule.GetParentWindow().IsValid())
	{
		FSlateApplication::Get().AddWindowAsNativeChild(CookbookWindow, MainFrameModule.GetParentWindow().ToSharedRef());
	}
	else
	{
		FSlateApplication::Get().AddWindow(CookbookWindow);
	}
}

//To see Editor changing it is needed to right-click Uproject and Generate Visual Studio project files before launching it again

void FChapter10_EditorModule::MyButton4_Clicked()
{
	TSharedRef<SWindow> CookbookWindow = SNew(SWindow)
		.Title(FText::FromString(TEXT("Cookbook Editor Window 4")))
		.ClientSize(FVector2D(700, 700))
		.SupportsMaximize(false)
		.SupportsMinimize(false)
		[
			SNew(SVerticalBox) + SVerticalBox::Slot()
			.HAlign(HAlign_Center)
		.VAlign(VAlign_Center)
		[
			SNew(STextBlock)
			.Text(FText::FromString(TEXT("Hello there from Slate again!!!!")))
		]
		];

	IMainFrameModule& MainFrameModule = FModuleManager::LoadModuleChecked<IMainFrameModule>(TEXT("MainFrame"));

	if (MainFrameModule.GetParentWindow().IsValid())
	{
		FSlateApplication::Get().AddWindowAsNativeChild(CookbookWindow, MainFrameModule.GetParentWindow().ToSharedRef());
	}
	else
	{
		FSlateApplication::Get().AddWindow(CookbookWindow);
	}
}

void FChapter10_EditorModule::AddToolbarExtension4(FToolBarBuilder& builder)
{
	FSlateIcon IconBrush = FSlateIcon(FEditorStyle::GetStyleSetName(), "LevelEditor.ViewOptions", "LevelEditor.ViewOptions.Small");
	builder.AddToolBarButton(FCookbookCommands::Get().MyButton4, NAME_None, FText::FromString("My Button"), FText::FromString("Click me to display a message"), IconBrush, NAME_None);
}

void FChapter10_EditorModule::DisplayWindow(FString WindowTitle)
{
	TSharedRef<SWindow> CookbookWindow = SNew(SWindow)
		.Title(FText::FromString(WindowTitle))
		.ClientSize(FVector2D(800, 400))
		.SupportsMaximize(false)
		.SupportsMinimize(false);

	IMainFrameModule& MainFrameModule = FModuleManager::LoadModuleChecked<IMainFrameModule>(TEXT("MainFrame"));

	if (MainFrameModule.GetParentWindow().IsValid())
	{
		FSlateApplication::Get().AddWindowAsNativeChild(CookbookWindow, MainFrameModule.GetParentWindow().ToSharedRef());
	}
	else
	{
		FSlateApplication::Get().AddWindow(CookbookWindow);
	}
}

//After Compiling and Generating Visual Studio project files, Play the level and hit the tilde key, in the console type your 'test' commands and Enter
//First re-open your uproject go to Project Setting->Input->Console->Console Keys and add key if the tilde key is unavailable on your keyboard
//In the console cmd type: DisplayTestCommand to see 'Test Command Window' or DisplayWindow mytestwindow1 to see 'mytestwindow1'