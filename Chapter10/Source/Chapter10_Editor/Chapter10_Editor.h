// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "Engine.h"
#include "Modules/ModuleInterface.h"
#include "Modules/ModuleManager.h"
#include "UnrealEd.h"
#include "CookbookCommands.h"
#include "Interfaces/IMainFrameModule.h"
#include "LevelEditor.h"
#include "SlateBasics.h"
#include "Framework/MultiBox/MultiBoxExtender.h"
#include "IAssetTypeActions.h"
#include "IAssetTools.h"
#include "AssetToolsModule.h"
#include "MyCustomAssetPinFactory.h"

class FChapter10_EditorModule: public IModuleInterface
{
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;

	TSharedPtr<FExtender> ToolbarExtender;
	TSharedPtr<const FExtensionBase> Extension;

	void MyButton_Clicked();

	void AddToolbarExtension(FToolBarBuilder& builder);

	void AddMenuExtension(FMenuBuilder& builder);

	TSharedPtr<FExtender> MenuExtender;
	TSharedPtr<const FExtensionBase> Extension2;

	void MyMenuButton_Clicked();



	void AddMenuExtension2(FMenuBuilder& builder);
	TSharedPtr<FExtender> MenuExtender2;
	TSharedPtr<const FExtensionBase> Extension3;
	void MyMenuButton2_Clicked();



	void AddToolbarExtension4(FToolBarBuilder& builder);
	TSharedPtr<FExtender> ToolbarExtender4;
	TSharedPtr<const FExtensionBase> Extension4;
	void MyButton4_Clicked();


	TArray< TSharedPtr<IAssetTypeActions> > CreatedAssetTypeActions;



	IConsoleCommand* DisplayTestCommand;
	IConsoleCommand* DisplayUserSpecifiedWindow;
	void DisplayWindow(FString WindowTitle);



	TSharedPtr<FMyCustomAssetPinFactory> PinFactory;
};