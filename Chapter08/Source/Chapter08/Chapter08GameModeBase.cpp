// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.


#include "Chapter08GameModeBase.h"
#include "Misc/OutputDeviceNull.h"


void AChapter08GameModeBase::BeginPlay()
{
	Super::BeginPlay();

	AChapter08GameModeBase* gm = Cast<AChapter08GameModeBase>(GetWorld()->GetAuthGameMode());

	if (gm)
	{
		if (UPBlueprintClassName != nullptr)
		{
			FTransform SpawnLocation;
			SpawnLocation.SetLocation(FVector(-1000, 300, 70));
			GetWorld()->SpawnActor<ATalkingMesh>(UPBlueprintClassName, SpawnLocation);
			FOutputDeviceNull ar;
			UPBlueprintClassName->GetDefaultObject()->CallFunctionByNameWithArguments(TEXT("StartTalking"), ar, NULL, true);
			GEngine->AddOnScreenDebugMessage(-1, 30, FColor::Green, "I'm the calling the Blueprint from code");
			UE_LOG(LogTemp, Warning, TEXT("I'm the calling the Blueprint from code"));
		}
	}
}