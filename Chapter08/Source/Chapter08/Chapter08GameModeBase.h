// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TalkingMesh.h"
#include "Chapter08GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CHAPTER08_API AChapter08GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:

	virtual void BeginPlay() override;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = UClassNames)
		TSubclassOf<ATalkingMesh> UPBlueprintClassName;
};
