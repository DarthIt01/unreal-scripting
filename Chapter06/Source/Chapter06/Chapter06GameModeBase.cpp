// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.


#include "Chapter06GameModeBase.h"
#include "Blueprint/UserWidget.h"

void AChapter06GameModeBase::BeginPlay()
{
	Super::BeginPlay();

	UUserWidget* Menu = CreateWidget<UUserWidget>(GetWorld(), Widget);

	if (Menu)
	{
		Menu->AddToViewport();
		GetWorld()->GetFirstPlayerController()->bShowMouseCursor = true;
	}
}

void AChapter06GameModeBase::ButtonClicked()
{
	UE_LOG(LogTemp, Warning, TEXT("UI Button Clicked"));
}

void AChapter06GameModeBase::SpellButtonClicked()
{
	UE_LOG(LogTemp, Warning, TEXT("Spell Button Clicked"));
}