// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Chapter06GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CHAPTER06_API AChapter06GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, Category = UIFuncs)
		void ButtonClicked();

	void BeginPlay();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "UI")
		TSubclassOf<class UUserWidget> Widget;

	UFUNCTION(BlueprintCallable, Category = UIFuncs)
		void SpellButtonClicked();
};
