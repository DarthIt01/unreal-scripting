// Fill out your copyright notice in the Description page of Project Settings.


#include "Warrior.h"
#include "Components/InputComponent.h"
#include "GameFramework/PlayerInput.h"
#include "Chapter06GameModeBase.h"
#include "Components/CapsuleComponent.h"


// Sets default values
AWarrior::AWarrior()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	lastInput = FVector2D::ZeroVector;
}

// Called when the game starts or when spawned
void AWarrior::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWarrior::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	float len = lastInput.Size();

	//If the player's input is greater than 1, normalize it
	if (len > 1.0f)
	{
		lastInput /= len;
	}

	AddMovementInput(GetActorForwardVector(), lastInput.Y);
	AddMovementInput(GetActorRightVector(), lastInput.X);

	//Zero off last input values
	lastInput = FVector2D(0.0f, 0.0f);
}

// Called to bind functionality to input
void AWarrior::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	check(PlayerInputComponent);
	PlayerInputComponent->BindAxis("Forward", this, &AWarrior::Forward);
	PlayerInputComponent->BindAxis("Back", this, &AWarrior::Back);
	PlayerInputComponent->BindAxis("Right", this, &AWarrior::Right);
	PlayerInputComponent->BindAxis("Left", this, &AWarrior::Left);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &AWarrior::Jump);

	//Only add PlayerInput header if:
	//Not using editor key binding and mapping directly from code, like below
	//Alternate keys for movement

	FInputAxisKeyMapping forwardKey("Forward1", EKeys::T, 1.0f);
	UPlayerInput::AddEngineDefinedAxisMapping(forwardKey);
	PlayerInputComponent->BindAxis("Forward1", this, &AWarrior::Forward);

	FInputAxisKeyMapping backKey("Back1", EKeys::G, 1.0f);
	UPlayerInput::AddEngineDefinedAxisMapping(backKey);
	PlayerInputComponent->BindAxis("Back1", this, &AWarrior::Back);

	FInputAxisKeyMapping rightKey("Right1", EKeys::H, 1.0f);
	UPlayerInput::AddEngineDefinedAxisMapping(rightKey);
	PlayerInputComponent->BindAxis("Right1", this, &AWarrior::Right);

	FInputAxisKeyMapping leftKey("Left1", EKeys::F, 1.0f);
	UPlayerInput::AddEngineDefinedAxisMapping(leftKey);
	PlayerInputComponent->BindAxis("Left1", this, &AWarrior::Left);

	FInputActionKeyMapping jumpKey("Jump1", EKeys::J, 0, 0, 0, 0);
	UPlayerInput::AddEngineDefinedActionMapping(jumpKey);
	PlayerInputComponent->BindAction("Jump1", IE_Pressed, this, &AWarrior::Jump);

	//Or FInputActionKeyMapping jumpKey("Jump1", EKeys::J, 0, 0, 0, 0);
	//and GetWorld()->GetFirstPlayerController()->PlayerInput->AddActionMapping(jumpKey);
	//and PlayerInputComponent->BindAction("Jump1", IE_Pressed, this, &AWarrior::Jump);

	FInputActionKeyMapping spellKey("HotKey_UIButton_Spell", EKeys::I, 0, 0, 0, 0);
	GetWorld()->GetFirstPlayerController()->PlayerInput->AddActionMapping(spellKey);

	//Calling function for HotKey
	auto GameMode = Cast<AChapter06GameModeBase>(GetWorld()->GetAuthGameMode());
	auto Func = &AChapter06GameModeBase::SpellButtonClicked;

	if (GameMode && Func)
	{
		PlayerInputComponent->BindAction("HotKey_UIButton_Spell", IE_Pressed, GameMode, Func);
	}
}

void AWarrior::Forward(float amount)
{
	//Moves the player forward by an amount in forward direction
	//AddMovementInput(GetActorForwardVector(), amount);

	//We use a += of the amount added so that the other function modifying .Y (::Back()) affects lastInput, it won't overwrite with 0's
	lastInput.Y += amount;
}

void AWarrior::Back(float amount)
{
	//Moves the player forward by an amount in forward direction
	//AddMovementInput(-GetActorForwardVector(), amount);

	//In this case we are using -= since we are moving backwards
	lastInput.Y -= amount;
}

void AWarrior::Right(float amount)
{
	//Moves the player forward by an amount in forward direction
	//AddMovementInput(GetActorRightVector(), amount);

	lastInput.X += amount;
}

void AWarrior::Left(float amount)
{
	//Moves the player forward by an amount in forward direction
	//AddMovementInput(-GetActorRightVector(), amount);

	lastInput.X -= amount;
}

void AWarrior::OnOverlapsBegin_Implementation(UPrimitiveComponent* Comp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	UE_LOG(LogTemp, Warning, TEXT("Overlaps warrior began"));
}

void AWarrior::OnOverlapsEnd_Implementation(UPrimitiveComponent* Comp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	UE_LOG(LogTemp, Warning, TEXT("Overlaps warrior ended"));
}

void AWarrior::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (RootComponent)
	{
		//Attach contact function to all bounding components
		GetCapsuleComponent()->OnComponentBeginOverlap.AddDynamic(this, &AWarrior::OnOverlapsBegin);
		GetCapsuleComponent()->OnComponentEndOverlap.AddDynamic(this, &AWarrior::OnOverlapsEnd);
	}
}