// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.


#include "Chapter03GameModeBase.h"

void AChapter03GameModeBase::BeginPlay()
{
	Super::BeginPlay();

	AChapter03GameModeBase* gm = Cast<AChapter03GameModeBase>(GetWorld()->GetAuthGameMode());

	if(gm)
	{
		//Create an object
		UAction* action = NewObject<UAction>((UObject*)GetTransientPackage(), UAction::StaticClass() /* RF_* flags */);
		
		//Destroy an object
		action->ConditionalBeginDestroy();
	}
}