// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Action.h"
#include "Chapter03GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CHAPTER03_API AChapter03GameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	
	void BeginPlay();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = UClassNames)
		TSubclassOf<UAction> UPBlueprintClassName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Properties)
		TArray< UAction* > ActionsArr;
};
