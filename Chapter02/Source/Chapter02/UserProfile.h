// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "ColoredTexture.h"
#include "EnumName.h"
#include "UserProfile.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class CHAPTER02_API UUserProfile : public UObject
{
	GENERATED_BODY()

public:
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats)
        float Armor;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats)
        float HpMax;

    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats)
        FString Name = "Billie";

    //Display any UClasses deriving from UObject in a dropdown menu in Blueprints
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Unit)
        TSubclassOf<UObject> UClassOfPlayer;

    //Display string names of UCLASSes that derive from the GameMode C++ base class
    UPROPERTY(EditAnywhere, meta = (MetaClass = "GameMode"), Category = Unit)
        FStringClassReference UClassGameMode;

    //Custom struct example
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = HUD)
        FColoredTexture Texture;

    //Custom enum struct example
    UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Statuos)
        TEnumAsByte<Status> status;
};
