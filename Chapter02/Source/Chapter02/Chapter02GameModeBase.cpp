// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.


#include "Chapter02GameModeBase.h"

void AChapter02GameModeBase::BeginPlay()
{
	Super::BeginPlay();

	AChapter02GameModeBase* gm = Cast<AChapter02GameModeBase>(GetWorld()->GetAuthGameMode());

	if(gm)
	{
		UUserProfile* newobject = NewObject<UUserProfile>((UObject*)GetTransientPackage(), UUserProfile::StaticClass());
		FString string = FString::Printf(TEXT( "Name from C++ class = %s "), *(newobject->Name)); //or *newobject->Name
		UE_LOG(LogTemp, Warning, TEXT("%s"), *string);
	}
}