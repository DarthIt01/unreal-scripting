// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.


#include "Chapter01GameModeBase.h"

void AChapter01GameModeBase::BeginPlay()
{
	Super::BeginPlay();

	//Basic UE_LOG message
	UE_LOG(LogTemp, Warning, TEXT("Some warning message"));

	//UE_LOG message with arguments
	int intVar = 5;
	float floatVar = 3.7f;
	FString fstringVar = "an fstring variable";
	UE_LOG(LogTemp, Warning, TEXT("Text, %d %f %s"), intVar, floatVar, *fstringVar);

	//UE_LOG message with FString argument concatenated using Printf
	FString name = "Tim";
	int32 mana = 450;
	FString string = FString::Printf(TEXT( "Name = %s Mana = %d"), *name, mana);
	UE_LOG(LogTemp, Warning, TEXT("%s"), *string);

	//UE_LOG message with FString argument concatenated using Format
	FString name2 = "Tim2";
	int32 mana2 = 700;
	TArray<FStringFormatArg> args;
	args.Add(FStringFormatArg(name2));
	args.Add(FStringFormatArg(mana2));
	FString string2 = FString::Format(TEXT("Name2 = {0} Mana2 = {1}"), args);
	UE_LOG(LogTemp, Warning, TEXT("%s"), *string2);

}

/*
Some Tips before starting a new game:

Visual Studio: Tools->Options->Text Editor->C/C++ tick Automatic brace completion
In ->Formatting tick all starting with "Automatically indent" or "Automatically format",
and When I paste choose Do nothing or leave it as Indent and/or Format 

When coding enable Autocomplete with: Ctrl + Spacebar

After including new modules to *.Build.cs or to Project->*Properties->NMake->Include Search Path,
Right-click your uproject and Generate Visual Studio project files, and
Re-open your project via Unreal Editor, go to File->Open Project, it will open it again

In VS, compile your project right-clicking it and choosing Build or Rebuild. In the Editor hit Compile;
Some BPs and other assets may need to be replaced or instanced again in the Viewport after changing
them in code

Rebuild may not work and sync with Editor may not occur if changing the editor itself, changing the code
or not re-opening the project properly. Usually the Editor doesn't even start and crashes, so the project 
have to be rebuild manually. Start by deleting Binaries, Intermediate and Saved folders of your project,
you can even have your *.sln or VS solution opened in the process, right-click your uproject and Generate
Visual Studio project files, the code will be reloaded and the folders recreated

UE Editor: Edit->Editor Preferences->Loading & Saving:
untick all items of Auto Save and Source Control(this will reduce the size of your project)
In Level Editor->Play enable Game Gets Mouse Control
*/
