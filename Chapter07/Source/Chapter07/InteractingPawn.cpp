// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractingPawn.h"
#include "Interactable.h"
#include "Camera/PlayerCameraManager.h"
#include "CollisionQueryParams.h"
#include "WorldCollision.h"
#include "Components/InputComponent.h"
#include "GameFramework/PlayerInput.h"

// Sets default values
AInteractingPawn::AInteractingPawn()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	lastInput = FVector2D::ZeroVector;
}

// Called when the game starts or when spawned
void AInteractingPawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AInteractingPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	float len = lastInput.Size();

	//If the player's input is greater than 1, normalize it
	if (len > 1.0f)
	{
		lastInput /= len;
	}

	AddMovementInput(GetActorForwardVector(), lastInput.Y);
	AddMovementInput(GetActorRightVector(), lastInput.X);

	//Zero off last input values
	lastInput = FVector2D(0.0f, 0.0f);
}

void AInteractingPawn::TryInteract()
{
	APlayerController* MyController = Cast<APlayerController>(Controller);

	if (MyController)
	{
		APlayerCameraManager* MyCameraManager = MyController->PlayerCameraManager;

		auto StartLocation = MyCameraManager->GetCameraLocation();
		auto EndLocation = StartLocation + (MyCameraManager->GetActorForwardVector() * 100);

		FCollisionObjectQueryParams Params;
		FHitResult HitResult;

		GetWorld()->SweepSingleByObjectType(HitResult, StartLocation, EndLocation, FQuat::Identity, FCollisionObjectQueryParams(FCollisionObjectQueryParams::AllObjects), FCollisionShape::MakeSphere(25), FCollisionQueryParams(FName("Interaction"), true, this));

		if (HitResult.Actor != nullptr)
		{
			auto Class = HitResult.Actor->GetClass();
			if (Class->ImplementsInterface(UInteractable::StaticClass()))
			{
				if (IInteractable::Execute_CanInteract(HitResult.Actor.Get()))
				{
					IInteractable::Execute_PerformInteract(HitResult.Actor.Get());
				}
			}
		}
	}
}

void AInteractingPawn::SetupPlayerInputComponent(UInputComponent* InInputComponent)
{
	Super::SetupPlayerInputComponent(InInputComponent);

	check(InInputComponent);

	FInputAxisKeyMapping forwardKey("Forward1", EKeys::T, 1.0f);
	UPlayerInput::AddEngineDefinedAxisMapping(forwardKey);
	InInputComponent->BindAxis("Forward1", this, &AInteractingPawn::Forward);

	FInputAxisKeyMapping backKey("Back1", EKeys::G, 1.0f);
	UPlayerInput::AddEngineDefinedAxisMapping(backKey);
	InInputComponent->BindAxis("Back1", this, &AInteractingPawn::Back);

	FInputAxisKeyMapping rightKey("Right1", EKeys::H, 1.0f);
	UPlayerInput::AddEngineDefinedAxisMapping(rightKey);
	InInputComponent->BindAxis("Right1", this, &AInteractingPawn::Right);

	FInputAxisKeyMapping leftKey("Left1", EKeys::F, 1.0f);
	UPlayerInput::AddEngineDefinedAxisMapping(leftKey);
	InInputComponent->BindAxis("Left1", this, &AInteractingPawn::Left);

	FInputActionKeyMapping interactKey("Interact", EKeys::I, 0, 0, 0, 0);
	UPlayerInput::AddEngineDefinedActionMapping(interactKey);
	InInputComponent->BindAction("Interact", IE_Released, this, &AInteractingPawn::TryInteract);
}

void AInteractingPawn::Forward(float amount)
{
	//Moves the player forward by an amount in forward direction
	//AddMovementInput(GetActorForwardVector(), amount);

	//We use a += of the amount added so that the other function modifying .Y (::Back()) affects lastInput, it won't overwrite with 0's
	lastInput.Y += amount;
}

void AInteractingPawn::Back(float amount)
{
	//Moves the player forward by an amount in forward direction
	//AddMovementInput(-GetActorForwardVector(), amount);

	//In this case we are using -= since we are moving backwards
	lastInput.Y -= amount;
}

void AInteractingPawn::Right(float amount)
{
	//Moves the player forward by an amount in forward direction
	//AddMovementInput(GetActorRightVector(), amount);

	lastInput.X += amount;
}

void AInteractingPawn::Left(float amount)
{
	//Moves the player forward by an amount in forward direction
	//AddMovementInput(-GetActorRightVector(), amount);

	lastInput.X -= amount;
}