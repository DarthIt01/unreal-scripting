// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Chapter07.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Chapter07, "Chapter07" );
