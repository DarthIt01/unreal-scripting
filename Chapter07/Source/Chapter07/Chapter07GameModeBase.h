// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MyInterface.h"
#include "Chapter07GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CHAPTER07_API AChapter07GameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
    
    virtual void BeginPlay() override;

    TArray<IMyInterface*> MyInterfaceInstances;
};
