// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DefaultPawn.h"
#include "InteractingPawn.generated.h"

/**
 * 
 */
UCLASS()
class CHAPTER07_API AInteractingPawn : public ADefaultPawn
{
	GENERATED_BODY()

	AInteractingPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//The movement from the previous frame
	FVector2D lastInput;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//Movement functions
	void Forward(float amount);

	void Back(float amount);

	void Right(float amount);

	void Left(float amount);

    void TryInteract();

private:
    virtual void SetupPlayerInputComponent(UInputComponent* InInputComponent) override;
};
