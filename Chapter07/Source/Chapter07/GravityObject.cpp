// Fill out your copyright notice in the Description page of Project Settings.


#include "GravityObject.h"

// Add default functionality here for any IGravityObject functions that are not pure virtual.

void IGravityObject::EnableGravity()
{
	AActor* ThisAsActor = Cast<AActor>(this);
	if (ThisAsActor != nullptr)
	{
		TArray<UPrimitiveComponent*> PrimitiveComponents;

		ThisAsActor->GetComponents(PrimitiveComponents);

		for (UPrimitiveComponent* Component : PrimitiveComponents)
		{
			Component->SetEnableGravity(true);
		}
		
		GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Red, TEXT("Enabling Gravity"));
	}
}

void IGravityObject::DisableGravity()
{
	AActor* ThisAsActor = Cast<AActor>(this);
	if (ThisAsActor != nullptr)
	{
		TArray<UPrimitiveComponent*> PrimitiveComponents;

		ThisAsActor->GetComponents(PrimitiveComponents);

		for (UPrimitiveComponent* Component : PrimitiveComponents)
		{
			Component->SetEnableGravity(false); //SetEnableGravity is a member of UPrimitive Component
			//Component->SetDisableGravity(true) won't work, SetDisableGravity is not a member of UPrimitive Component
			//and EnableGravity(), DisableGravity() are just methods of your interface
		}
		
		GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Red, TEXT("Disabling Gravity"));
	}
}