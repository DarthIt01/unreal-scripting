// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.


#include "Chapter07GameModeBase.h"
#include "SingleInterfaceActor.h"
#include "EngineUtils.h" //TActorIterator


void AChapter07GameModeBase::BeginPlay()
{
	Super::BeginPlay();

	//Spawn a new actor using the ASingleInterfaceActor class at the default location
	FTransform SpawnLocation;
	ASingleInterfaceActor* SpawnedActor = GetWorld()->SpawnActor<ASingleInterfaceActor>(ASingleInterfaceActor::StaticClass(), SpawnLocation);

	//Get a reference to the class the actor has
	UClass* ActorClass = SpawnedActor->GetClass();

	//If the class implements the interface, display a message
	if (ActorClass->ImplementsInterface(UMyInterface::StaticClass()))
	{
		GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Red, TEXT("Spawned actor implements interface!"));
	}

	//Spawn two new actors using the ASingleInterfaceActor class at the default location
	FTransform SpawnLocation2;
	ASingleInterfaceActor* SpawnedActor2 = GetWorld()->SpawnActor<ASingleInterfaceActor>(ASingleInterfaceActor::StaticClass(), SpawnLocation2);

	FTransform SpawnLocation3;
	ASingleInterfaceActor* SpawnedActor3 = GetWorld()->SpawnActor<ASingleInterfaceActor>(ASingleInterfaceActor::StaticClass(), SpawnLocation3);

	for (TActorIterator<AActor> It(GetWorld(), AActor::StaticClass()); It; ++It)
	{
		AActor* Actor = *It;

		IMyInterface* MyInterfaceInstance = Cast<IMyInterface>(Actor);

		if (MyInterfaceInstance)
		{
			MyInterfaceInstances.Add(MyInterfaceInstance);
		}
	}
	
	//Print out how many objects implement the interface
	FString Message = FString::Printf(TEXT("%d actors implement the interface"), MyInterfaceInstances.Num());

	GEngine->AddOnScreenDebugMessage(-1, 10, FColor::Red, Message);
}