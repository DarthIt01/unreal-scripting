// Fill out your copyright notice in the Description page of Project Settings.


#include "ScalingPlayerController.h"
#include "SlateBasics.h"

void AScalingPlayerController::BeginPlay()
{
	Super::BeginPlay();
	TSharedRef<SVerticalBox> widget = SNew(SVerticalBox)
		+ SVerticalBox::Slot()
		.HAlign(HAlign_Center)
		.VAlign(VAlign_Center)
		[
			SNew(SButton)
			.Content()
		[
			SNew(STextBlock)
			.Text(FText::FromString(TEXT("Test button")))
		]
		];
	GEngine->GameViewport->AddViewportWidgetForPlayer(GetLocalPlayer(), widget, 1);
	//To change the size resolution go Edit->Project Settings->Engine->User Interface->DPI Scaling->DPI Curve and select the first and second dots and set Scale to 1.0

}