// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SlateGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CHAPTER14_API ASlateGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	ASlateGameModeBase();
};
