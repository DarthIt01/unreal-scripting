// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "Chapter14.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( Chapter14Module, Chapter14, "Chapter14" );
