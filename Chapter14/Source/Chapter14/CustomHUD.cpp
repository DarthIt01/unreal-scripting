// Fill out your copyright notice in the Description page of Project Settings.


#include "CustomHUD.h"
#include "Engine/Canvas.h"

void ACustomHUD::DrawHUD()
{
	Super::DrawHUD();
	
	Canvas->DrawText(GEngine->GetSmallFont(), TEXT("Test string to be printed to the screen"), 10.0f, 10.0f, 2.0f, 2.0f);
	
	FCanvasBoxItem ProgressBar(FVector2D(5.0f, 50.0f), FVector2D(200.0f, 10.0f));
	
	Canvas->DrawItem(ProgressBar);
	
	DrawRect(FLinearColor::Blue, 5.0f, 100.0f, 200.0f, 10.0f);

	FCanvasBoxItem ProgressBar2(FVector2D(5.0f, 150.0f), FVector2D(200.0f, 10.0f));
	
	Canvas->DrawItem(ProgressBar2);
	
	DrawRect(FLinearColor::Red, 5.0f, 150.0f, 200.0f, 10.0f);
}