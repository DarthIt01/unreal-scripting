// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/Widget.h"
#include "SCustomButton.h"
#include "Framework/SlateDelegates.h"
#include "CustomButtonWidget.generated.h"

DECLARE_DYNAMIC_DELEGATE_RetVal(FString, FGetString);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FButtonClicked);

/**
 * 
 */
UCLASS()
class CHAPTER14_API UCustomButtonWidget : public UWidget
{
	GENERATED_BODY()

protected:
    TSharedPtr<SCustomButton> MyButton;

    virtual TSharedRef<SWidget> RebuildWidget() override;

public:
    
    UCustomButtonWidget();

    //multicast
    UPROPERTY(BlueprintAssignable)
        FButtonClicked ButtonClicked;

    FReply OnButtonClicked();

    UPROPERTY(BlueprintReadWrite, EditAnywhere)
        FString Label;

    //must be of the form varNameDelegate
    UPROPERTY()
        FGetString LabelDelegate;

    virtual void SynchronizeProperties() override;
};
