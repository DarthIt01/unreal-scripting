// Fill out your copyright notice in the Description page of Project Settings.


#include "CustomGameModeBase.h"
#include "CustomHUD.h"

ACustomGameModeBase::ACustomGameModeBase() : AGameModeBase()
{
	HUDClass = ACustomHUD::StaticClass();
}