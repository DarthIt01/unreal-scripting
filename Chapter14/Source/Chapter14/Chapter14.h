// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "CookbookStyle.h"

class Chapter14Module : public FDefaultGameModuleImpl
{
	virtual void StartupModule() override
	{
		FCookbookStyle::Initialize();
	}

	virtual void ShutdownModule() override
	{
		FCookbookStyle::Shutdown();
	}
};