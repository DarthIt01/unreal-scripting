// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ScalingGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CHAPTER14_API AScalingGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	AScalingGameModeBase();
};
