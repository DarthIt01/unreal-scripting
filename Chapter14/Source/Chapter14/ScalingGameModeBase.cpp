// Fill out your copyright notice in the Description page of Project Settings.


#include "ScalingGameModeBase.h"
#include "ScalingPlayerController.h"

AScalingGameModeBase::AScalingGameModeBase() : AGameModeBase()
{
	PlayerControllerClass = AScalingPlayerController::StaticClass();
	//To change the size resolution go Edit->Project Settings->Engine->User Interface->DPI Scaling->DPI Curve and select the first and second dots and set Scale to 1.0
	
	//Alternatively open the Config folder DefaultEngine.ini [/Script/Engine.UserInterfaceSettings] section and set Value=1.000000 of EditorCurveData Keys Time resolutions
	
	//It's also possible to change resolutions in Play mode, selecting New Editor Window to the right of Play's toolbar
	//open the command console and type r.SetRes widthxheight, example: r.SetRes 800x600 
}