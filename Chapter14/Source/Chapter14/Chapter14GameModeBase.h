// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Chapter14GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CHAPTER14_API AChapter14GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
