// Fill out your copyright notice in the Description page of Project Settings.


#include "SlateGameModeBase.h"
#include "CustomPlayerController.h"

ASlateGameModeBase::ASlateGameModeBase() : Super()
{
	PlayerControllerClass = ACustomPlayerController::StaticClass();
}