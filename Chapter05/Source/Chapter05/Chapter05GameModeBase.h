// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Chapter05GameModeBase.generated.h"

/**
 * 
 */
DECLARE_DELEGATE(FStandardDelegateSignature)
DECLARE_DELEGATE_OneParam(FParamDelegateSignature, FLinearColor)
DECLARE_MULTICAST_DELEGATE(FMulticastDelegateSignature)
UCLASS()
class CHAPTER05_API AChapter05GameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
    FStandardDelegateSignature MyStandardDelegate;

    FParamDelegateSignature MyParameterDelegate;

    FMulticastDelegateSignature MyMulticastDelegate;
};
