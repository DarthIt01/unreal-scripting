// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Chapter09GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class CHAPTER09_API AChapter09GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
