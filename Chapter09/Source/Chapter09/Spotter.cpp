// Fill out your copyright notice in the Description page of Project Settings.


#include "Spotter.h"
#include "UObject/ConstructorHelpers.h"
#include "DrawDebugHelpers.h"

ASpotter::ASpotter()
{
	PrimaryActorTick.bCanEverTick = true;

	auto MeshAsset = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("StaticMesh'/Engine/BasicShapes/Cone.Cone'"));

	UStaticMeshComponent* SM = GetStaticMeshComponent();

	if (SM != nullptr)
	{
		if (MeshAsset.Object != nullptr)
		{
			SM->SetStaticMesh(MeshAsset.Object);
			SM->SetGenerateOverlapEvents(true);
		}

		SM->SetMobility(EComponentMobility::Movable);
		SM->SetRelativeRotation(FRotator(0, 0, 270));

	}
	
}

void ASpotter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	auto EndLocation = GetActorLocation() + ActorToWorld().TransformVector(FVector(0, 0, -3000));

	//Check if there is an object in front of us
	FHitResult HitResult;
	GetWorld()->SweepSingleByChannel(HitResult, GetActorLocation(), EndLocation, FQuat::Identity, ECC_Camera, FCollisionShape::MakeSphere(25), FCollisionQueryParams("Spot", true, this));

	APawn* SpottedPlayer = Cast<APawn>(HitResult.Actor.Get());

	//If there is, call the OnPlayerSpotted function
	if (SpottedPlayer != nullptr)
	{
		OnPlayerSpotted(SpottedPlayer);
	}

	//Displays where we are checking for collision
	DrawDebugLine(GetWorld(), GetActorLocation(), EndLocation, FColor::Red);
}