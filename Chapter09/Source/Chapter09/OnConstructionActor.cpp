// Fill out your copyright notice in the Description page of Project Settings.


#include "OnConstructionActor.h"
#include "UObject/ConstructorHelpers.h"

AOnConstructionActor::AOnConstructionActor()
{
	PrimaryActorTick.bCanEverTick = true;

	auto MeshAsset = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("StaticMesh'/Engine/BasicShapes/Cone.Cone'"));

	UStaticMeshComponent* SM = GetStaticMeshComponent();

	if (SM != nullptr)
	{
		if (MeshAsset.Object != nullptr)
		{
			SM->SetStaticMesh(MeshAsset.Object);
			SM->SetGenerateOverlapEvents(true);
		}

		SM->SetMobility(EComponentMobility::Movable);

	}
	
	//Default value of property
	ShowStaticMesh = true;

}

void AOnConstructionActor::OnConstruction(const FTransform& Transform)
{
	//Hit play, then Shift+F1, then select OnConstructionActor or PostEditChangePropertyActor to change their locations on runtime
	//change either x's translation values of both and note that after toggling visibility only PostEditChangePropertyActor disappears
	GetStaticMeshComponent()->SetVisibility(ShowStaticMesh);

	Super::OnConstruction(Transform);
}