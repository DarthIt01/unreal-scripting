// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Chapter12GameMode.generated.h"

UCLASS(minimalapi)
class AChapter12GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AChapter12GameMode();
};



