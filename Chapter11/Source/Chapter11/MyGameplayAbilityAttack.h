// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"
#include "MyGameplayAbilityAttack.generated.h"

/**
 * 
 */
UCLASS()
class CHAPTER11_API UMyGameplayAbilityAttack : public UGameplayAbility
{
	GENERATED_BODY()

public:
	//Returns true if this ability can be activated right now, has no side effects
	virtual bool CanActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayTagContainer* SourceTags = nullptr, const FGameplayTagContainer* TargetTags = nullptr, OUT FGameplayTagContainer* OptionalRelevantTags = nullptr) const
	{
		UE_LOG(LogTemp, Warning, TEXT("Attack Ability can be activated!"));
		return true;
	}

	//Check cost, returns true if this ability can be paid for, false if not
	virtual bool CheckCost(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, OUT FGameplayTagContainer* OptionalRelevantTags = nullptr) const
	{
		UE_LOG(LogTemp, Warning, TEXT("Attack Ability checkcost!"));
		return true;
		//return Super::CheckCost(Handle, ActorInfo, OptionalRelevantTags);
	}

	//Activate ability
	virtual void ActivateAbility(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo, const FGameplayEventData* TriggerEventData)
	{
		UE_LOG(LogTemp, Warning, TEXT("Activating Attack Ability swings weapon!"));
		Super::ActivateAbility(Handle, ActorInfo, ActivationInfo, TriggerEventData);
	}

	//Input binding stub
	virtual void InputPressed(const FGameplayAbilitySpecHandle Handle, const FGameplayAbilityActorInfo* ActorInfo, const FGameplayAbilityActivationInfo ActivationInfo)
	{
		UE_LOG(LogTemp, Warning, TEXT("Attack Ability input pressed!"));
		Super::InputPressed(Handle, ActorInfo, ActivationInfo);
	}
};
