// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTask.h"
#include "Particles/ParticleSystem.h"
#include "GameplayTask_CreateParticles.generated.h"

/**
 * 
 */
UCLASS()
class CHAPTER11_API UGameplayTask_CreateParticles : public UGameplayTask
{
	GENERATED_BODY()

public:
    virtual void Activate();

    //Static constructor of UGameplayTask_CreateEmitter instance including class of emitter and where to create it arguments
    UFUNCTION(BlueprintCallable, Category = "GameplayTasks", meta = (AdvancedDisplay = "TaskOwner", DefaultToSelf = "TaskOwner", BlueprintInternalUseOnly = "TRUE"))
        static UGameplayTask_CreateParticles* ConstructTask(TScriptInterface<IGameplayTaskOwnerInterface> TaskOwner, UParticleSystem* ParticleSystem, FVector Location);

    UParticleSystem* ParticleSystem;

    FVector Location;
};
