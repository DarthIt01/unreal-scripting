// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GameplayAbilitySet.h"
#include "AbilitySystemInterface.h"
#include "GameplayTask.h"
#include "Warrior.generated.h"

#define FS(x,...) FString::Printf( TEXT( x ), __VA_ARGS__ )

UCLASS()
class CHAPTER11_API AWarrior : public ACharacter, public IAbilitySystemInterface, public IGameplayTaskOwnerInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AWarrior();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//The movement from the previous frame
	FVector2D lastInput;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//Movement functions
	void Forward(float amount);

	void Back(float amount);

	void Right(float amount);

	void Left(float amount);

	//Lists key triggers for various abilities for the player, Selects an instance of UGameplayAbilitySet, which is a UDataAsset derivative that you construct in the Content Browser
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats)
		UGameplayAbilitySet* gameplayAbilitySet;

	//The AbilitySystemComponent itself
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats)
		UAbilitySystemComponent* abilitySystemComponent;

	//IAbilitySystemInterface implementation:
	virtual UAbilitySystemComponent* GetAbilitySystemComponent() const { return abilitySystemComponent; }

	virtual void PostInitializeComponents() override;

	void TestGameplayEffect();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats)
		UGameplayTasksComponent* gameplayTasksComponent;

	//The particleSystem created with GameplayTask_CreateParticles object
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats)
		UParticleSystem* particleSystem;

	//GameplayTaskOwnerInterface implementation:
	virtual UGameplayTasksComponent* GetGameplayTasksComponent(const UGameplayTask& Task) const { return gameplayTasksComponent; }

	//Called both when task starts and when task gets resume, to differentiate use Task.GetStatus()
	virtual void OnTaskActivated(UGameplayTask& Task) { ; }

	virtual void OnTaskDeactivated(UGameplayTask& Task) { ; }

	//Give an accurate answer for the Task
	virtual AActor* GetOwnerActor(const UGameplayTask* Task) const { return Task->GetOwnerActor(); }
};