// Fill out your copyright notice in the Description page of Project Settings.


#include "Warrior.h"
#include "Components/InputComponent.h"
#include "GameFramework/PlayerInput.h"
#include "AbilitySystemComponent.h"
#include "Abilities/GameplayAbility.h"
#include "GameUnitAttributeSet.h"
#include "GameplayTask_CreateParticles.h"

// Sets default values
AWarrior::AWarrior()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	lastInput = FVector2D::ZeroVector;
	
	abilitySystemComponent = CreateDefaultSubobject<UAbilitySystemComponent>("UAbilitySystemComponent");
	gameplayTasksComponent = CreateDefaultSubobject<UGameplayTasksComponent>("UGameplayTasksComponent");
}

// Called when the game starts or when spawned
void AWarrior::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWarrior::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	float len = lastInput.Size();

	//If the player's input is greater than 1, normalize it
	if (len > 1.0f)
	{
		lastInput /= len;
	}

	AddMovementInput(GetActorForwardVector(), lastInput.Y);
	AddMovementInput(GetActorRightVector(), lastInput.X);

	//Zero off last input values
	lastInput = FVector2D(0.0f, 0.0f);
}

// Called to bind functionality to input
void AWarrior::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	check(PlayerInputComponent);

	//Only add PlayerInput header if:
	//Not using editor key binding and mapping directly from code, like below
	//Alternate keys for movement

	FInputAxisKeyMapping forwardKey("Forward1", EKeys::T, 1.0f);
	UPlayerInput::AddEngineDefinedAxisMapping(forwardKey);
	PlayerInputComponent->BindAxis("Forward1", this, &AWarrior::Forward);

	FInputAxisKeyMapping backKey("Back1", EKeys::G, 1.0f);
	UPlayerInput::AddEngineDefinedAxisMapping(backKey);
	PlayerInputComponent->BindAxis("Back1", this, &AWarrior::Back);

	FInputAxisKeyMapping rightKey("Right1", EKeys::H, 1.0f);
	UPlayerInput::AddEngineDefinedAxisMapping(rightKey);
	PlayerInputComponent->BindAxis("Right1", this, &AWarrior::Right);

	FInputAxisKeyMapping leftKey("Left1", EKeys::F, 1.0f);
	UPlayerInput::AddEngineDefinedAxisMapping(leftKey);
	PlayerInputComponent->BindAxis("Left1", this, &AWarrior::Left);

	FInputActionKeyMapping jumpKey("Jump1", EKeys::J, 0, 0, 0, 0);
	UPlayerInput::AddEngineDefinedActionMapping(jumpKey);
	PlayerInputComponent->BindAction("Jump1", IE_Pressed, this, &AWarrior::Jump);



	//Connect the class's AbilitySystemComponent to the actor's input component
	abilitySystemComponent->BindToInputComponent(PlayerInputComponent);

	//Go through each BindInfo in the gameplayAbilitySet, give & try and activate each on the AbilitySystemComponent
	for(const FGameplayAbilityBindInfo& BindInfo : gameplayAbilitySet->Abilities)
	{
		//Gets an instance of the UClass
		FGameplayAbilitySpec spec(BindInfo.GameplayAbilityClass->GetDefaultObject<UGameplayAbility>(), 1, (int32)BindInfo.Command);

		//Store the ability handle for later invocation of the ability
		FGameplayAbilitySpecHandle abilityHandle = abilitySystemComponent->GiveAbility(spec);

		//The integer id that invokes the ability (ith) value in enum listing
		int32 abilityID = (int32)BindInfo.Command;

		//Construct the InputBinds object, which will allow us to wire-up an input event to the InputPressed() / InputReleased() events of the GameplayAbility
		FGameplayAbilityInputBinds inputBinds(FS("ConfirmingTargetting_%s_%s", *GetName(), *BindInfo.GameplayAbilityClass->GetName()), FS("CancelTargetting_%s_%s", *GetName(), *BindInfo.GameplayAbilityClass->GetName()), "EGameplayAbilityInputBinds", abilityID, abilityID);

		//Bind each ability to the inputComponent
		abilitySystemComponent->BindAbilityActivationToInputComponent(PlayerInputComponent, inputBinds);

		//Test-kicks the ability to active state
		abilitySystemComponent->TryActivateAbility(abilityHandle, 1);
	}
}

void AWarrior::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (abilitySystemComponent)
	{
		abilitySystemComponent->InitStats(UGameUnitAttributeSet::StaticClass(), NULL);
	}



	UGameplayTask_CreateParticles* task = UGameplayTask_CreateParticles::ConstructTask(this, particleSystem, FVector(200.0f, 0.0f, 200.0f));

	if (gameplayTasksComponent && task)
	{
		gameplayTasksComponent->AddTaskReadyForActivation(*task);
	}
}

void AWarrior::Forward(float amount)
{
	//Moves the player forward by an amount in forward direction
	//AddMovementInput(GetActorForwardVector(), amount);

	//We use a += of the amount added so that the other function modifying .Y (::Back()) affects lastInput, it won't overwrite with 0's
	lastInput.Y += amount;
}

void AWarrior::Back(float amount)
{
	//Moves the player forward by an amount in forward direction
	//AddMovementInput(-GetActorForwardVector(), amount);

	//In this case we are using -= since we are moving backwards
	lastInput.Y -= amount;
}

void AWarrior::Right(float amount)
{
	//Moves the player forward by an amount in forward direction
	//AddMovementInput(GetActorRightVector(), amount);

	lastInput.X += amount;
}

void AWarrior::Left(float amount)
{
	//Moves the player forward by an amount in forward direction
	//AddMovementInput(-GetActorRightVector(), amount);

	lastInput.X -= amount;
}

inline UGameplayEffect* ConstructGameplayEffect(FString name)
{
	return NewObject<UGameplayEffect>(GetTransientPackage(), FName(*name));
}

inline FGameplayModifierInfo& AddModifier(UGameplayEffect* Effect, UProperty* Property, EGameplayModOp::Type Op, const FGameplayEffectModifierMagnitude& Magnitude)
{
	int32 index = Effect->Modifiers.Num();
	Effect->Modifiers.SetNum(index + 1);
	FGameplayModifierInfo& Info = Effect->Modifiers[index];
	Info.ModifierMagnitude = Magnitude;
	Info.ModifierOp = Op;
	Info.Attribute.SetUProperty(Property);
	return Info;
}

void AWarrior::TestGameplayEffect()
{
	//Construct and retrieve UProperty to affect
	UGameplayEffect* RecoverMana = ConstructGameplayEffect("RecoverMana");

	//Compile-time checked retrieval of Mana UPROPERTY() from our UGameUnitAttributeSet class
	UProperty* manaProperty = FindFieldChecked<UProperty>(UGameUnitAttributeSet::StaticClass(), GET_MEMBER_NAME_CHECKED(UGameUnitAttributeSet, Mana));

	//Command the addition +100 mana to the manaProperty
	AddModifier(RecoverMana, manaProperty, EGameplayModOp::Additive, FScalableFloat(100.0f));

	//for a fixed duration of 3 secs
	RecoverMana->DurationPolicy = EGameplayEffectDurationType::HasDuration;
	RecoverMana->DurationMagnitude = FScalableFloat(3.0f);

	//with 100% chance of success
	RecoverMana->ChanceToApplyToTarget = 1.0f;
	
	//and recurrency (period) of 0.5 seconds
	RecoverMana->Period = 0.5f;
	
	//Apply the effect to an AbilitySystemComponent
	FActiveGameplayEffectHandle recoverManaEffectHandle = abilitySystemComponent->ApplyGameplayEffectToTarget(RecoverMana, abilitySystemComponent, 1.0f);

	//or remove the effect attaching a function delegate to its handle
	/*
	FOnActiveGameplayEffectRemoved* ep = abilitySystemComponent->OnGameplayEffectRemovedDelegate(recoverManaEffectHandle);
	if (ep)
	{
		ep->AddLambda([]()
			{
				UE_LOG(LogTemp, Warning, TEXT("Recover effect has been removed"), 1);
			});
	}
	*/
}