// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.


#include "Chapter11GameModeBase.h"
#include "Chapter11.h"
#include "GameplayTagsModule.h"
#include "GameplayTagsManager.h"

void AChapter11GameModeBase::BeginPlay()
{
	//Traditional Logging
	UE_LOG(LogTemp, Warning, TEXT("Message %d"), 1);

	//Our custom log type
	UE_LOG(LogCh11, Display, TEXT("A display message, log is working")); //shows in gray
	UE_LOG(LogCh11, Warning, TEXT("A warning message"));
	UE_LOG(LogCh11, Error, TEXT("An error message"));

	//Messages to the Message Log
	CreateLog(LoggerName);
	//Retrieve the log by using the LoggerName
	FMessageLog logger(LoggerName);
	logger.Warning(FTEXT("A warning message from gamemode"));
	logger.Info(FTEXT("Info to log"));
	logger.Error(FTEXT("Error text to log"));

	//TestHttp();
}

void AChapter11GameModeBase::TestHttp()
{
	//Create the IHttpRequest object from your FHttpModule singleton interface
	TSharedRef<IHttpRequest>
		http = FHttpModule::Get().CreateRequest();

	//Display progressbar for longer http requests
	http->OnRequestProgress().BindLambda(
		[this](FHttpRequestPtr request, int32 sentBytes, int32 receivedBytes)->void
		{
			int32 contentLen = request->GetResponse()->GetContentLength();
			float percentComplete = 100.0f * receivedBytes / contentLen;

			UE_LOG(LogTemp, Warning, TEXT("Progress sent=%d bytes / received=%d/%d bytes [%.0f%%]"), sentBytes, receivedBytes, contentLen, percentComplete);
		}
	);

	//When the HTTP request completes bind to the delegate below in one of various ways:
	FHttpRequestCompleteDelegate& delegate = http->OnProcessRequestComplete();

	//1.BindLambda():
	delegate.BindLambda(
		[](FHttpRequestPtr request, FHttpResponsePtr response, bool success)->void
		{
			UE_LOG(LogTemp, Warning, TEXT("Http response %d, %s"), response->GetResponseCode(), *response->GetContentAsString());
		}
	);

	//2.BindRaw(), have to be sure to delete p later, but not until callback completes:
	PlainObject* p = new PlainObject();
	delegate.BindRaw(p, &PlainObject::httpHandler);

	//3.BindSP()
	TSharedRef<PlainObject> sr(new PlainObject());
	delegate.BindSP(sr, &PlainObject::httpHandler);

	//4.BindThreadSafeSP()
	TSharedRef<SharedObject, ESPMode::ThreadSafe> tssr(new SharedObject());
	delegate.BindThreadSafeSP(tssr, &SharedObject::httpHandler);

	//5.BindStatic()
	delegate.BindStatic(&httpHandler);

	//6.BindUFunction(), can't use for functions that have non-UCLASS, USTRUCT or UENUM type arguments (ie no tsharedrefs)
	//delegate.BindUFunction(this, &AChapter11GameModeBase::HttpRequestComplete);

	//7.BindUObject()
	delegate.BindUObject(this, &AChapter11GameModeBase::HttpRequestComplete);

	//Set the web address to open by setting the URL of the HttpRequest
	http->SetURL(TEXT("https://docs.unrealengine.com/"));

	http->ProcessRequest();
}

void AChapter11GameModeBase::HttpRequestComplete(FHttpRequestPtr request, FHttpResponsePtr response, bool success)
{
	//Print the http response
	UE_LOG(LogTemp, Warning, TEXT("Http response %d, %s"), response->GetResponseCode(), *response->GetContentAsString());
}