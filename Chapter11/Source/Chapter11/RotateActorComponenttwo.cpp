// Fill out your copyright notice in the Description page of Project Settings.


#include "RotateActorComponenttwo.h"

// Sets default values for this component's properties
URotateActorComponenttwo::URotateActorComponenttwo()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void URotateActorComponenttwo::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void URotateActorComponenttwo::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	FQuat quat(FVector(0, 1, 0), GetWorld()->TimeSeconds * PI / 4.0f);
	FQuat quattwo(FVector(1, 0, 0), GetWorld()->TimeSeconds * PI / 4.0f);
	//GetOwner()->SetActorRotation(quattwo);
	GetOwner()->SetActorRotation(quat);
	//GetOwner()->SetActorRotation(quattwo*quat);
}

